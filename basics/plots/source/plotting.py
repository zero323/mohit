# Databricks notebook source
# MAGIC %md 
# MAGIC 
# MAGIC # Plotting
# MAGIC 
# MAGIC __Important__: Almost all topics covered here are not directly connected to Spark. There are specific to either Databricks properitary platfrom, their own plotting system (similar to Apache Zeppelin), as well as integration with 3rd party libraries.

# COMMAND ----------

import requests
import zipfile
import os
import sys

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/00235/household_power_consumption.zip"
local_path = "/dbfs/FileStore/household_power_consumption.txt"
path = "/FileStore/household_power_consumption.txt"

if not os.path.exists(local_path):
  with open("/tmp/household_power_consumption.zip", 'wb') as fw:
    r = requests.get(url, stream = True)
    for chunk in r.iter_content(chunk_size = 4096):
      fw.write(chunk)
      
  with zipfile.ZipFile("household_power_consumption.zip") as zf:
    zf.extract("household_power_consumption.txt", local_path)
    
dbutils.fs.ls(path)

# COMMAND ----------

from pyspark.sql.functions import (
  col, concat_ws, unix_timestamp,
  year, month, dayofmonth, hour, minute,
  date_format
)
from pyspark.sql.types import *
from functools import partial

# COMMAND ----------

columns = [
	'date', 'time',
	'global_active_power',	'global_reactive_power',
	'voltage', 'global_intensity', 'sub_metering_1',
	'sub_metering_2', 'sub_metering_3'
]

schema = StructType([
    StructField(c.lower(), DoubleType() if c not in {"date", "time"} else StringType(), True)
    for c in columns
])

funs = {
  "year": year,
  "month": month,
  "dayofmonth": dayofmonth,
  "hour": hour,
  "minute": minute,
  "dayofweek": partial(date_format, format="u")
}

ts = unix_timestamp(concat_ws(" ", "date", "time"), "dd/MM/yyyy HH:mm").cast("timestamp")
exprs = ["*"] + [f("timestamp").alias(a) for a, f in funs.items()]

df = (spark.read
      .options(inferSchema="true", header="true", delimiter=";", nullValue="?")
      .schema(schema)
      .csv(path)
      .withColumn("timestamp", ts)
      .drop("Date", "Time")
      .select(exprs)
      .na.drop()
      .cache())

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ## Databricks `display` system

# COMMAND ----------

display(df)

# COMMAND ----------

from pyspark.sql.functions import min as min_, max as max_, stddev, avg

grp_cols = ["dayofweek", "hour"]

agg_funs = [min_, max_, stddev, avg]
agg_cols = 	[
  'global_active_power', 'global_reactive_power',
  'voltage', 'global_intensity', 'sub_metering_1',
  'sub_metering_2', 'sub_metering_3'
]
agg_exprs = [f(c).alias("{}_{}".format(f.__name__, c)) for f in agg_funs for c in agg_cols]

aggregated = df.groupBy(grp_cols).agg(*agg_exprs)

aggregated.count()

# COMMAND ----------

display(aggregated)  # Be careful. Order in Spark is usually nondeterministic.

# COMMAND ----------

display(aggregated.orderBy("hour"))

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ## [Bokeh](http://bokeh.pydata.org/en/latest/)
# MAGIC 
# MAGIC Bokeh is an amazing library, originally developed by [Continuum Analytics](http://continuum.io/). It can be used to create interactive dashboards in pure Python. 
# MAGIC 
# MAGIC Since it is not supported out-of-the-box on Databricks, we have to create a small wrapper, which uses HTML dispaly system.

# COMMAND ----------

import bokeh

from bokeh.charts import BoxPlot
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.io import save
import tempfile

def displayBokeh(p):
  script, div = components(p)  # Extracts html components
  f = tempfile.mktemp()        # Writes to temporary file
  save(p, f)                     
  with open(f) as fr:          # Reads file
    html = fr.read() 
  os.unlink(f)                 # Removes temporary file
  displayHTML(html)            # Renders the output

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC Number of observations is unifrom across grouping variables so we can use standard sampling, instead of stratified one

# COMMAND ----------

display(df.groupBy("dayofweek", "hour").count().orderBy("dayofweek", "hour"))

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC One use can for Pandas (or other local data structures) is plotting. Beware that it `collects` data to the driver, so everything has to fit in the memory of a single machine (in this case it is not an issue, and we sample only to ilustrate the point).

# COMMAND ----------

sampled = df.sample(False, 0.05)
pdf = sampled.toPandas()  # Local object

pdf[:10]

# COMMAND ----------

# MAGIC %md
# MAGIC 
# MAGIC __Note__: 
# MAGIC   
# MAGIC Rendering in IFrame is a bit unreliable. You may have resize the outupt, to actually see any plot.

# COMMAND ----------

# TODO: explore bokeh.embed.notebook_div
displayBokeh(BoxPlot(pdf, values="global_active_power", label="hour", legend=False, plot_width=960))

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ## [seaborn](http://seaborn.pydata.org/)
# MAGIC 
# MAGIC Seaborn is Python plotting library based on [Matplotlib](http://matplotlib.org/). It provides a lot interesting plots, with very sensible visuals.

# COMMAND ----------

import seaborn as sns
sns.plt.close()

f, ax = sns.plt.subplots(figsize=(24, 8))

p = sns.violinplot(x = "hour", y = "sub_metering_3", data = pdf, ax=ax)
display(p.figure)

# COMMAND ----------

pdf[pdf["dayofmonth"] ]

# COMMAND ----------

import pandas as pd 

sns.plt.close()

pdf_long = pd.melt(pdf, id_vars = ["month", "dayofmonth", "hour"], value_vars = ["sub_metering_1", "sub_metering_2", "sub_metering_3"])

g = sns.FacetGrid(pdf_long, col="variable",  row="dayofmonth", size=8)
g = g.map(sns.boxplot, "hour", "value")
display(g.fig)

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ## [plotly](https://plot.ly/)
# MAGIC 
# MAGIC Is a hosted, commercial platform for hosting interactive plots. It provides a number of language bindings, and some supprort for local (offline) plots.

# COMMAND ----------

# Here we'll use aggregated data.

aggregated_rows = aggregated.collect()  # This is plain Python list of Rows
aggregated_rows[:5]

# COMMAND ----------

import plotly
import plotly.graph_objs as go

# We could use Pandas here as well
data = [
    go.Scatter(
        x = [row["hour"] for row in aggregated_rows],  # Rows can be accessed using square brackets (getitem)
        y = [row.avg_global_active_power for row in aggregated_rows], # You can also use dot syntax (getattr)
        mode="markers"
    )
]

# Same as with Bokey we have to use HTML rendering component
displayHTML(plotly.offline.plot(data, output_type = "div"))

# COMMAND ----------

# MAGIC %md 
# MAGIC 
# MAGIC ## Plotting with R
# MAGIC 
# MAGIC Since Databricks (similarly to Zeppelin), supports multilingual notebooks, we can `registerTempView` and plot data using one of awesome R libraries.

# COMMAND ----------

sampled.createOrReplaceTempView("df")
spark.catalog.cacheTable("df")

# COMMAND ----------

# MAGIC %r
# MAGIC library(magrittr)
# MAGIC library(dplyr)
# MAGIC if (!require(tidyr)) {
# MAGIC   install.packages("tidyr")
# MAGIC }
# MAGIC 
# MAGIC 
# MAGIC df <- SparkR::sql("SELECT * FROM df") %>% 
# MAGIC   SparkR::collect()  # Once again we collect, here to local R data.frame
# MAGIC   

# COMMAND ----------

# MAGIC %r
# MAGIC library(ggplot2)
# MAGIC 
# MAGIC df_long <- df %>% gather("metric", "value", matches("global_.*_power"))
# MAGIC 
# MAGIC p <- ggplot(df_long, aes(hour, value, colour = factor(month))) + geom_smooth() + facet_grid(metric ~ .)
# MAGIC p
