# Databricks notebook source
from __future__ import print_function
from pyspark.sql.functions import col, udf, struct
from pyspark.sql import Column
from pyspark.sql.types import *
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoder, VectorAssembler, RFormula, IndexToString, VectorIndexer, SQLTransformer, QuantileDiscretizer
from pyspark.ml.linalg import Vectors, VectorUDT, SparseVector, DenseVector

from operator import add, itemgetter
from itertools import chain
import json

import pandas as pd
import numpy as np
from sklearn.calibration import calibration_curve

def explain(features, metadata):
	mapping = {x["idx"]: x["name"] for x in chain(*metadata['ml_attr']['attrs'].values())}
	if isinstance(features, SparseVector):
		idx = features.indices
		name =[mapping[int(i)] for i in features.indices]
		value = [features[int(i)] for i in features.indices]

	elif isinstance(features, DenseVector):
		idx = np.arange(len(features))
		name = [mapping[int(i)] for i in range(len(features))]
		value = [features[int(i)] for i in range(len(features))]
	
	else:
		raise ValueError()
	

	return pd.DataFrame({"idx": idx, "name": name, "value": value}).sort_values("value", ascending=False)
  
  
from pyspark.sql.types import DoubleType
from pyspark.sql.functions import lit, udf

def ith_(v, i):
    try:
        return float(v[i])
    except ValueError:
        return None

ith = udf(ith_, DoubleType())


def calibration_curve_(predictions, fraction=0.1, n_bins=10):
    from sklearn.calibration import calibration_curve
  
    labels, probs = zip(*predictions.select(col("label").cast("integer"), ith("probability", lit(1))).sample(False, fraction).collect())
    fraction_of_positives, mean_predicted_value = calibration_curve(labels, probs, n_bins=n_bins)
    return spark.createDataFrame(pd.DataFrame({"fraction_of_positives": fraction_of_positives, "mean_predicted_value":  mean_predicted_value}))

# COMMAND ----------

# MAGIC %md ## Logistic regression with ridge reg.

# COMMAND ----------

trainDF = spark.table("linear_pruned_train")
testDF = spark.table("linear_pruned_test")

trainDF

# COMMAND ----------

from pyspark.ml.classification import LogisticRegression
from pyspark.ml.evaluation import MulticlassClassificationEvaluator

evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction", metricName="accuracy")
lr = LogisticRegression(labelCol="label", featuresCol="features", maxIter=50, elasticNetParam=0, regParam=0.01)

pipeline = Pipeline(stages=[lr])

# Train model with Training Data
lr_pipeline_model = pipeline.fit(trainDF)

# Make predictions on test data using the transform() method.
# LogisticRegression.transform() will only use the 'features' column.
lr_predictions = lr_pipeline_model.transform(testDF)


lr_accuracy = evaluator.evaluate(lr_predictions)

print("Test Error = %5.2f%%" % ((1.0 - lr_accuracy) * 100))

# COMMAND ----------

display(lr_predictions.crosstab("label", "prediction"))

# COMMAND ----------

display(
  spark.createDataFrame(enumerate(lr_pipeline_model.stages[-1].summary.objectiveHistory),
  ("loss", "iteration")))

# COMMAND ----------

display(spark.createDataFrame(explain(
      lr_pipeline_model.stages[-1].coefficients,
      lr_predictions.select("features").schema[0].metadata)))

# COMMAND ----------

display(calibration_curve_(lr_predictions))

# COMMAND ----------

# MAGIC %md ## Logistic regression with lasso reg.

# COMMAND ----------

from pyspark.ml.classification import LogisticRegression

lr_lasso = LogisticRegression(labelCol="label", featuresCol="features", maxIter=50, elasticNetParam=1, regParam=0.01)
lr_lasso_pipeline = Pipeline(stages=[lr_lasso])

# Train model with Training Data
lr_lasso_pipeline_model = lr_lasso_pipeline.fit(trainDF)

# Make predictions on test data using the transform() method.
# LogisticRegression.transform() will only use the 'features' column.
lr_lasso_predictions = lr_lasso_pipeline_model.transform(testDF)


lr_lasso_accuracy = evaluator.evaluate(lr_lasso_predictions)

print("Test Error = %5.2f%%" % ((1.0 - lr_lasso_accuracy) * 100))

# COMMAND ----------

display(lr_lasso_predictions.crosstab("label", "prediction"))

# COMMAND ----------

display(
  spark.createDataFrame(enumerate(lr_lasso_pipeline_model.stages[-1].summary.objectiveHistory),
  ("loss", "iteration")))

# COMMAND ----------

display(spark.createDataFrame(explain(
      lr_lasso_pipeline_model.stages[-1].coefficients,
      lr_lasso_predictions.select("features").schema[0].metadata)))

# COMMAND ----------

# MAGIC %md ## Logistic regression with elastic net regularization

# COMMAND ----------

from pyspark.ml.classification import LogisticRegression

lr_elastic_net = LogisticRegression(labelCol="label", featuresCol="features", maxIter=10, elasticNetParam=0.5, regParam=0.01)
lr_elastic_net_pipeline = Pipeline(stages=[lr_elastic_net])

# Train model with Training Data
lr_elastic_net_pipeline_model = lr_elastic_net_pipeline .fit(trainDF)

# Make predictions on test data using the transform() method.
# LogisticRegression.transform() will only use the 'features' column.
lr_elastic_net_predictions = lr_elastic_net_pipeline_model.transform(testDF)


lr_elastic_net_accuracy = evaluator.evaluate(lr_elastic_net_predictions)

print("Test Error = %5.2f%%" % ((1.0 - lr_elastic_net_accuracy) * 100))

# COMMAND ----------

display(lr_elastic_net_predictions.crosstab("label", "prediction"))

# COMMAND ----------

display(calibration_curve_(lr_elastic_net_predictions))

# COMMAND ----------

display(
  spark.createDataFrame(enumerate(lr_elastic_net_pipeline_model.stages[-1].summary.objectiveHistory),
  ("loss", "iteration")))

# COMMAND ----------

display(spark.createDataFrame(explain(
      lr_elastic_net_pipeline_model.stages[-1].coefficients,
      lr_elastic_net_predictions.select("features").schema[0].metadata)))
