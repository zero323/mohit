# Databricks notebook source
from __future__ import print_function

# COMMAND ----------

# MAGIC %md # Feature engineering

# COMMAND ----------

# MAGIC %md ## Helpers

# COMMAND ----------

from pyspark.ml.feature import *
from pyspark.ml import Pipeline

def feature_pipeline(df, label_input_col, ohe_categorical=False,
                     discretize_and_ohe_numeric=False, num_buckets=5,
                     label_output_col="label", features_output_col="features",
                     exclude=None):
    """
    A helper function which generates simple feature transformation pipelines

    - For linear models:
        ohe_categorical=True, discretize_and_ohe_numeric=False (similar to RFormula)
    - For tree models:
        ohe_categorical=False, discretize_and_ohe_numeric=True or False
    - For Naive Bayes:
        ohe_categorical=True, discretize_and_ohe_numeric=True

    :param df: DataFrame
    :param label_input_col: str name of the raw label column
    :param ohe_categorical: bool should we ohe categorical columns
    :param discretize_and_ohe_numeric: bool should we quantile discretize and ohe numeric
    :param num_buckets: int used as a parameter for quantile discretizer
    :param label_output_col: str name for the encoded label
    :param features_output_col: str name for the assembled features
    :param exclude: List[str] names of the columns that should be ignored
    :return: Pipeline
    """
    exclude = set(exclude or []) | {label_input_col}

    def cols_for_type(t, df=df, exclude=exclude):
        return [f.name for f in df.schema if isinstance(f.dataType, t) and f.name not in exclude]

    def output_cols(transformers):
        return [t.getOutputCol() for t in transformers]

    float_cols = cols_for_type((FloatType, DoubleType, DecimalType))
    integer_cols = cols_for_type((IntegerType, LongType))
    string_cols = cols_for_type(StringType)


    # Index strings (categorical) str -> double
    indexers = [
        StringIndexer(inputCol=c, outputCol="{0}_idx".format(c), handleInvalid="skip")
            for c in string_cols]

    # Convert continuous numerical variables
    bucketizers = ([
        QuantileDiscretizer(numBuckets=num_buckets, inputCol=c, outputCol="{}_disc".format(c))
        for c in float_cols + integer_cols
    ] if discretize_and_ohe_numeric else [])

    # Dummy encode columns double -> vector {0, 1}
    encoders = [
        OneHotEncoder(inputCol=c, outputCol="{}_enc".format(c)) for c in
        output_cols(
            (indexers if ohe_categorical else []) +
            (bucketizers if discretize_and_ohe_numeric else []))
    ]


    # Assemble multiple columns into a single vector
    assembler_cols = {
        (True, True): output_cols(encoders),
        (False, True): output_cols(indexers + encoders),
        (True, False): output_cols(encoders) + float_cols + integer_cols,
        (False, False): output_cols(indexers) + float_cols + integer_cols
    }

    assembler = VectorAssembler(
        inputCols=assembler_cols[(ohe_categorical, discretize_and_ohe_numeric)],
        outputCol=features_output_col)

    # Index labels
    label_indexer = StringIndexer(inputCol=label_input_col, outputCol=label_output_col)

    return Pipeline(stages=indexers + bucketizers + encoders + [assembler, label_indexer])

# COMMAND ----------

from pyspark import keyword_only, SparkContext
from pyspark.sql.functions import col, udf, struct
from pyspark.sql import Column
from pyspark.sql.types import *
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoder, VectorAssembler, RFormula, IndexToString, VectorIndexer, SQLTransformer, QuantileDiscretizer
from pyspark.ml.linalg import Vectors, VectorUDT, SparseVector, DenseVector

from operator import add, itemgetter
from itertools import chain
import json

import pandas as pd
import numpy as np

def explain(features, metadata):
	mapping = {x["idx"]: x["name"] for x in chain(*metadata['ml_attr']['attrs'].values())}
	if isinstance(features, SparseVector):
		idx = features.indices
		name =[mapping[int(i)] for i in features.indices]
		value = [features[int(i)] for i in features.indices]

	elif isinstance(features, DenseVector):
		idx = np.arange(len(features))
		name = [mapping[int(i)] for i in range(len(features))]
		value = [features[int(i)] for i in range(len(features))]
	
	else:
		raise ValueError()
	

	return pd.DataFrame({"idx": idx, "name": name, "value": value}).sort_values("value", ascending=False)
  
  
def feature_pipeline(df, label_input_col, ohe_categorical=False,
                     discretize_and_ohe_numeric=False, num_buckets=5,
                     label_output_col="label", features_output_col="features",
                     exclude=None):
    """
    A helper function which generates simple feature transformation pipelines

    - For linear models:
        ohe_categorical=True, discretize_and_ohe_numeric=False (similar to RFormula)
    - For tree models:
        ohe_categorical=False, discretize_and_ohe_numeric=True or False
    - For Naive Bayes:
        ohe_categorical=True, discretize_and_ohe_numeric=True

    :param df: DataFrame
    :param label_input_col: str name of the raw label column
    :param ohe_categorical: bool should we ohe categorical columns
    :param discretize_and_ohe_numeric: bool should we quantile discretize and ohe numeric
    :param num_buckets: int used as a parameter for quantile discretizer
    :param label_output_col: str name for the encoded label
    :param features_output_col: str name for the assembled features
    :param exclude: List[str] names of the columns that should be ignored
    :return: Pipeline
    """
    exclude = set(exclude or []) | {label_input_col}

    def cols_for_type(t, df=df, exclude=exclude):
        return [f.name for f in df.schema if isinstance(f.dataType, t) and f.name not in exclude]

    def output_cols(transformers):
        return [t.getOutputCol() for t in transformers]

    float_cols = cols_for_type((FloatType, DoubleType, DecimalType))
    integer_cols = cols_for_type((IntegerType, LongType))
    string_cols = cols_for_type(StringType)


    # Index strings (categorical) str -> double
    indexers = [
        StringIndexer(inputCol=c, outputCol="{0}_idx".format(c), handleInvalid="skip")
            for c in string_cols]

    # Convert continuous numerical variables
    bucketizers = ([
        QuantileDiscretizer(numBuckets=num_buckets, inputCol=c, outputCol="{}_disc".format(c))
        for c in float_cols + integer_cols
    ] if discretize_and_ohe_numeric else [])

    # Dummy encode columns double -> vector {0, 1}
    encoders = [
        OneHotEncoder(inputCol=c, outputCol="{}_enc".format(c)) for c in
        output_cols(
            (indexers if ohe_categorical else []) +
            (bucketizers if discretize_and_ohe_numeric else []))
    ]


    # Assemble multiple columns into a single vector
    assembler_cols = {
        (True, True): output_cols(encoders),
        (False, True): output_cols(indexers + encoders),
        (True, False): output_cols(encoders) + float_cols + integer_cols,
        (False, False): output_cols(indexers) + float_cols + integer_cols
    }

    assembler = VectorAssembler(
        inputCols=assembler_cols[(ohe_categorical, discretize_and_ohe_numeric)],
        outputCol=features_output_col)

    # Index labels
    label_indexer = StringIndexer(inputCol=label_input_col, outputCol=label_output_col)

    return Pipeline(stages=indexers + bucketizers + encoders + [assembler, label_indexer])

# COMMAND ----------

# MAGIC %md ## Data Loading

# COMMAND ----------

spark.sql("REFRESH TABLE loans_clean")
loans = spark.table("loans_clean").where(col("loan_status") != "PAID").repartition(48)

# COMMAND ----------

# MAGIC %md Create the train and test DataFrames 

# COMMAND ----------

from pyspark.sql.functions import *

trainDF, testDF = loans.randomSplit([0.7,0.3], 0)
trainDF.cache

print("We have %d training examples and %d test examples." % (trainDF.count(), testDF.count()))

# COMMAND ----------

# MAGIC %md ## Prepare datasets

# COMMAND ----------

# MAGIC %md For tree models we can use indexed categorical columns directly. Discretization can be useful but it is not strictly necessary

# COMMAND ----------

tree_features = feature_pipeline(loans, label_input_col="loan_status", exclude=["id"]).fit(trainDF)
tree_features.transform(trainDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("tree_train")
tree_features.transform(testDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("tree_test")

# COMMAND ----------

tree_features_pruned = feature_pipeline(loans, label_input_col="loan_status", exclude=["id", "out_prncp", "recoveries", "collection_recovery_fee", "out_prncp_inv"]).fit(trainDF)
tree_features_pruned.transform(trainDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("tree_pruned_train")
tree_features_pruned.transform(testDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("tree_pruned_test")

# COMMAND ----------

# MAGIC %md Linear models require indexing and enocding (OHE, dummy) of categorical variables. Numerical ones can be used directly

# COMMAND ----------

linear_features = feature_pipeline(loans, label_input_col="loan_status", ohe_categorical=True, exclude=["id"]).fit(trainDF)
linear_features.transform(trainDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("linear_train")
linear_features.transform(testDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("linear_test")

# COMMAND ----------

linear_pruned_features = feature_pipeline(loans, label_input_col="loan_status", ohe_categorical=True, exclude=["id", "out_prncp", "recoveries", "collection_recovery_fee", "out_prncp_inv", "issued_d"]).fit(trainDF)
linear_pruned_features.transform(trainDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("linear_pruned_train")
linear_pruned_features.transform(testDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("linear_pruned_test")

# COMMAND ----------

# MAGIC %md Naive Bayes considers only binary inputs (0, not 0), so it is necessary to encode categorical variables, and discretize + encode numerical ones.

# COMMAND ----------

nb_features = feature_pipeline(loans, label_input_col="loan_status", ohe_categorical=True, discretize_and_ohe_numeric=True, exclude=["id"]).fit(trainDF)
nb_features.transform(trainDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("nb_train")
nb_features.transform(testDF).select("id", "label", "features").write.format("parquet").mode("overwrite").saveAsTable("nb_test")


# COMMAND ----------

# MAGIC %md ## Simplified feature generation with `RFormula`

# COMMAND ----------

# MAGIC %md `RFormula` is suitable for linear models, should be acceptable, though inefficient for tree models, and a really bad choice for NB

# COMMAND ----------

from pyspark.ml.feature import RFormula

formula = "{} ~ {} - 1".format("loan_status", " + ".join(c for c in loans.columns if c not in {"id", "loan_status"}))
print(formula)

# COMMAND ----------

# MAGIC %md Unfortunately it is not as customizable as other transformer, for example it cannot handle unseen labels.

# COMMAND ----------

# MAGIC %md ## Feature interactions

# COMMAND ----------

# MAGIC %md [`Interaction`](https://spark.apache.org/docs/latest/ml-features.html#interaction), [`PolynomialExpansion`](https://spark.apache.org/docs/latest/ml-features.html#polynomialexpansion) and [`RFormula`](https://spark.apache.org/docs/latest/ml-features.html#rformula) can be used to including interactions / polynomial terms.
