# Databricks notebook source
# MAGIC %md ## Exploratory analysis

# COMMAND ----------

spark.sql("REFRESH TABLE loans_clean")
spark.table("loans_clean").printSchema()

# COMMAND ----------

display(spark.table("loans_clean"))

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Confirm the number of rows and total loan balance
# MAGIC SELECT count(*) as NumLoans,format_number(sum(loan_amnt),2) as TotalLoans from loans_clean

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Review the public records for delinquent lines 
# MAGIC select pub_rec, count(*) as c from loans_clean group by pub_rec order by pub_rec

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Review the open credit lines for delinquent lines 
# MAGIC select open_acc, count(*) as c from loans_clean where loan_status = 'DELINQUENT' group by open_acc order by open_acc

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Review the delinquency in the last 2 years 
# MAGIC select delinq_2yrs, count(*) as c from loans_clean where loan_status = 'DELINQUENT' group by delinq_2yrs order by delinq_2yrs

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Review the inquiry in the last 6 months 
# MAGIC select inq_last_6mths, count(*) as c from loans_clean where loan_status = 'DELINQUENT' group by inq_last_6mths order by inq_last_6mths

# COMMAND ----------

# This should work, but there seems to be some bug in the notebook

from pyspark.sql.functions import mean, count

# select loan_status, count(*) as NumLoans, AVG(CAST(loan_status = 'DELINQUENT' AS double)), sum(loan_amnt) as LoanBal 
# from loans_clean group by loan_status WITH ROLLUP

# display(spark.table("loans_clean").rollup("loan_status").agg(mean((col("loan_status") == "DELINQUENT").cast("double")), count("*")))

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Review the number of delinquencies in the loan portfolio
# MAGIC -- This should be done with rollup, but it looks like it is broken
# MAGIC 
# MAGIC select loan_status, count(*) as NumLoans, AVG(CAST(loan_status = 'DELINQUENT' AS double)) as DELINQUENT_FRACTION, sum(loan_amnt) as LoanBal, avg(loan_amnt)
# MAGIC from loans_clean group by loan_status
# MAGIC union all
# MAGIC select NULL, count(*) as NumLoans, AVG(CAST(loan_status = 'DELINQUENT' AS double)), sum(loan_amnt) as LoanBal, avg(loan_amnt) from loans_clean 

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Review the grade of delinquent loans
# MAGIC select grade, count(*), sum(loan_amnt) from loans_clean where loan_status = 'DELINQUENT' group by grade order by grade

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Review the grade / sub_grade of delinquent loans
# MAGIC select grade, sub_grade, count(*), sum(loan_amnt) from loans_clean where loan_status = 'DELINQUENT' group by grade, sub_grade, sub_grade  order by grade, sub_grade

# COMMAND ----------

# MAGIC %sql
# MAGIC --Review the geographic distribution of delinquent loans
# MAGIC select addr_state, count(*) as NumLoans, sum(loan_amnt) as LoanBal from loans_clean where loan_status = 'DELINQUENT' group by addr_state order by addr_state

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Graphical distribution of the number delinquent loans by state 
# MAGIC select addr_state, count(*) as NumLoans from loans_clean where loan_status = 'DELINQUENT' group by addr_state order by addr_state

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Graphical distribution of the delinquent loan balance by state 
# MAGIC select addr_state, sum(loan_amnt) as LoanBal from loans_clean where loan_status = 'DELINQUENT' group by addr_state order by addr_state

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Delinquencies by verification status
# MAGIC select verification_status, sum(loan_amnt) as LoanBal from loans_clean group by verification_status order by verification_status

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Delinquencies by length of employment
# MAGIC select emp_length, sum(loan_amnt) as LoanBal from loans_clean
# MAGIC group by emp_length order by emp_length

# COMMAND ----------

# MAGIC %sql
# MAGIC select application_type, log(count(*)) AS log_count from loans_clean
# MAGIC group by  application_type
