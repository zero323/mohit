# Databricks notebook source
from __future__ import print_function
from pyspark.sql.functions import col, udf, struct
from pyspark.sql import Column
from pyspark.sql.types import *
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoder, VectorAssembler, RFormula, IndexToString, VectorIndexer, SQLTransformer, QuantileDiscretizer
from pyspark.ml.linalg import Vectors, VectorUDT, SparseVector, DenseVector

from operator import add, itemgetter
from itertools import chain
import json

import pandas as pd
import numpy as np

def explain(features, metadata):
	mapping = {x["idx"]: x["name"] for x in chain(*metadata['ml_attr']['attrs'].values())}
	if isinstance(features, SparseVector):
		idx = features.indices
		name =[mapping[int(i)] for i in features.indices]
		value = [features[int(i)] for i in features.indices]

	elif isinstance(features, DenseVector):
		idx = np.arange(len(features))
		name = [mapping[int(i)] for i in range(len(features))]
		value = [features[int(i)] for i in range(len(features))]
	
	else:
		raise ValueError()
	

	return pd.DataFrame({"idx": idx, "name": name, "value": value}).sort_values("value", ascending=False)

# COMMAND ----------

# MAGIC %md ## Random Forest
# MAGIC 
# MAGIC For RF we only index nominal columns

# COMMAND ----------

trainDF = spark.table("tree_pruned_train")
testDF = spark.table("tree_pruned_test")

# COMMAND ----------

from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator, BinaryClassificationEvaluator
from pyspark.ml import Pipeline

rf = RandomForestClassifier(labelCol="label", featuresCol="features", numTrees=5, maxBins=51, maxDepth=3)
evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction", metricName="accuracy")

# Train model with Training Data
rf_pipeline = Pipeline(stages = [rf])
rf_pipeline_model = rf_pipeline.fit(trainDF)

# Make predictions on test data using the Transformer.transform() method
rf_predictions = rf_pipeline_model.transform(testDF)

rf_accuracy = evaluator.evaluate(rf_predictions)

print("Test Error = %5.2f%%" % ((1.0 - rf_accuracy) * 100))

# COMMAND ----------

display(spark.createDataFrame(explain(
       rf_pipeline_model.stages[-1].featureImportances,
       rf_predictions.select("features").schema[0].metadata)))

# COMMAND ----------

display(rf_predictions.crosstab("label", "prediction"))

# COMMAND ----------

# MAGIC %md ## Decision tree model

# COMMAND ----------

from pyspark.ml.classification import DecisionTreeClassifier

dt = DecisionTreeClassifier(labelCol="label", featuresCol="features", maxDepth=5, maxBins=51)
dt_pipeline = Pipeline(stages=[dt])

# Train model with Training Data
dt_pipeline_model = dt_pipeline.fit(trainDF)

# Make predictions on test data using the Transformer.transform() method.
dt_predictions = dt_pipeline_model.transform(testDF)

dt_accuracy = evaluator.evaluate(dt_predictions)

print("Test Error = %5.2f%%" % ((1.0 - dt_accuracy) * 100))

# COMMAND ----------

display(dt_predictions.crosstab("label", "prediction"))
