# Databricks notebook source
# MAGIC %md ## Baseline model

# COMMAND ----------

trainDF = spark.table("linear_train")
testDF = spark.table("linear_test")

# COMMAND ----------

from pyspark.ml.feature import SQLTransformer
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import MulticlassClassificationEvaluator

evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction", metricName="accuracy")

dummy_classifier_statement = """
  SELECT *, CAST(0.0 AS double) AS prediction -- Majority label will be indexed as 0
   FROM __THIS__"""
dummy_classifier = SQLTransformer(statement=dummy_classifier_statement)

baseline_predictions = Pipeline(stages=[dummy_classifier]).fit(trainDF).transform(testDF)
baseline_accuracy = evaluator.evaluate(baseline_predictions)

print("Test Error = %5.2f%%" % ((1.0 - baseline_accuracy) * 100))
