# Databricks notebook source
from __future__ import print_function
from pyspark.sql.functions import col, udf, struct
from pyspark.sql import Column
from pyspark.sql.types import *
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoder, VectorAssembler, RFormula, IndexToString, VectorIndexer, SQLTransformer, QuantileDiscretizer
from pyspark.ml.linalg import Vectors, VectorUDT, SparseVector, DenseVector

from operator import add, itemgetter
from itertools import chain
import json

import pandas as pd
import numpy as np

def explain(features, metadata):
	mapping = {x["idx"]: x["name"] for x in chain(*metadata['ml_attr']['attrs'].values())}
	if isinstance(features, SparseVector):
		idx = features.indices
		name =[mapping[int(i)] for i in features.indices]
		value = [features[int(i)] for i in features.indices]

	elif isinstance(features, DenseVector):
		idx = np.arange(len(features))
		name = [mapping[int(i)] for i in range(len(features))]
		value = [features[int(i)] for i in range(len(features))]
	
	else:
		raise ValueError()
	

	return pd.DataFrame({"idx": idx, "name": name, "value": value}).sort_values("value", ascending=False)

# COMMAND ----------

# MAGIC %md ## Naive Bayes

# COMMAND ----------

trainDF = spark.table("nb_train")
testDF = spark.table("nb_test")

# COMMAND ----------

# MAGIC %md ## NAIVE BAYES CLASSIFER - Laplace smoothing

# COMMAND ----------

from pyspark.ml.classification import NaiveBayes, NaiveBayesModel
from pyspark.ml.evaluation import MulticlassClassificationEvaluator

evaluator = MulticlassClassificationEvaluator(labelCol="label", predictionCol="prediction", metricName="accuracy")
nb = NaiveBayes(smoothing=1.0, modelType="multinomial", featuresCol="features")

nb_pipeline = Pipeline(stages=[nb])

nb_pipeline_model = nb_pipeline.fit(trainDF)

nb_predictions = nb_pipeline_model.transform(testDF)

nb_accuracy = evaluator.evaluate(nb_predictions)

print("Test Error = %5.2f%%" % ((1.0 - nb_accuracy) * 100))

# COMMAND ----------

theta = nb_pipeline_model.stages[-1].theta

# COMMAND ----------

explain(Vectors.dense(theta.toArray()[0]), nb_predictions.select("features").schema[0].metadata)

# COMMAND ----------

explain(Vectors.dense(theta.toArray()[1]), nb_predictions.select("features").schema[0].metadata)

# COMMAND ----------

display(nb_predictions.crosstab("label", "prediction"))

# COMMAND ----------

# MAGIC %md ## NAIVE BAYES CLASSIFER - Weighted

# COMMAND ----------

# MAGIC %md Compute weights for each class

# COMMAND ----------

del_frac, ndel_frac = trainDF.select(col("label"), 1 - col("label")).groupBy().mean().first()
del_weight = ndel_frac / del_frac

# COMMAND ----------

from pyspark.ml.feature import  SQLTransformer

weight_statement = """
  SELECT *, CASE
    WHEN label = 1 THEN {} 
    ELSE 1.0 
   END AS weight 
   FROM __THIS__""".format(del_weight)

# Add weight column which can be used to handle label skew
weight_adder = SQLTransformer(statement=weight_statement)


# COMMAND ----------

nb_weighted = NaiveBayes(smoothing=1.0, modelType="multinomial", featuresCol="features", weightCol="weight")

nb_weighted_pipeline = Pipeline(stages=[weight_adder, nb_weighted])

nb_weighted_pipeline_model = nb_weighted_pipeline.fit(trainDF)

nb_weighted_predictions = nb_weighted_pipeline_model.transform(testDF)

nb_weighted_accuracy = evaluator.evaluate(nb_weighted_predictions)

print("Test Error = %5.2f%%" % ((1.0 - nb_weighted_accuracy) * 100))

# COMMAND ----------

display(nb_weighted_predictions.crosstab("label", "prediction"))

# COMMAND ----------

# MAGIC %md ## Naive Bayes with Chi Sq feature selection

# COMMAND ----------

from pyspark.ml.feature import ChiSqSelector

selector = ChiSqSelector(numTopFeatures=21, featuresCol="features", outputCol="features_selected", labelCol="label")

nb_selected = NaiveBayes(smoothing=1.0, modelType="multinomial", featuresCol="features_selected")

nb_selected_pipeline = Pipeline(stages=[selector, nb_selected])

nb_selected_pipeline_model = nb_selected_pipeline.fit(trainDF)
nb_selected_predictions = nb_selected_pipeline_model.transform(testDF)
nb_selected_accuracy = evaluator.evaluate(nb_selected_predictions)

print("Test Error = %5.2f%%" % ((1.0 - nb_selected_accuracy) * 100))

# COMMAND ----------

# MAGIC %md Converges to baseline model

# COMMAND ----------

display(nb_selected_predictions.crosstab("label", "prediction"))  # Actually a baseline model

# COMMAND ----------

nb_selected_predictions.select("features_selected").schema.fields[0].metadata
