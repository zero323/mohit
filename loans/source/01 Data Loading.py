# Databricks notebook source
from __future__ import print_function, division

# COMMAND ----------

# Set the input path

path = "/FileStore/tables/i64zjvqa1496685595746/loan.csv"

# COMMAND ----------

# LOAD THE DATA
raw = (spark.read
      .format("csv")
      .option("header", "true")
      .option("delimiter", ",")
      .load(path))                 # NOTE: Parametrize with path

raw.printSchema()

# COMMAND ----------

print(len(raw.columns))

# COMMAND ----------

# MAGIC %md Cast columns to desired types

# COMMAND ----------

from pyspark.sql.functions import col

#Drop the columns that cannot be used
drop_cols = [
    "member_id", "emp_title", "url","desc", "title", "zip_code", "initial_list_status", "last_pymnt_d", 
    "last_pymnt_amnt", "last_credit_pull_d", "next_pymnt_d", "last_credit_pull_d", "collections_12_mths_ex_med", "policy_code",
    "open_acc_6m", "open_il_6m", "open_il_12m", "open_il_24m", "mths_since_rcnt_il", "open_rv_12m", "open_rv_24m", "all_util",
    "total_cu_tl", "verification_status_joint", "dti_joint", "annual_inc_joint", "il_util", "inq_last_12m", "inq_fi",
    "max_bal_bc", "total_bal_il",    "mths_since_last_record" , "earliest_cr_line"
]

#Clean the string columns
string_cols = [
    "grade", "sub_grade", "term", "addr_state", "home_ownership", "loan_status", "pymnt_plan", "verification_status",
     "emp_length","purpose", "application_type", "issue_d"
]

#Convert these columns to integer 
int_cols = ["delinq_2yrs", "inq_last_6mths", "open_acc", "pub_rec", "acc_now_delinq", "mths_since_last_delinq"]

#Convert these columns to double 
double_cols = [
    "loan_amnt", "funded_amnt", "funded_amnt_inv", "int_rate", "installment", "annual_inc", 
    "dti", "total_acc", "revol_bal", "revol_util", "total_rec_prncp", "total_rec_int",
    "recoveries", "tot_coll_amt", "tot_cur_bal", "total_rev_hi_lim",  "total_rec_late_fee", "out_prncp",
    "out_prncp_inv", "total_pymnt","total_pymnt_inv","collection_recovery_fee", "mths_since_last_major_derog"
]

# COMMAND ----------

display(raw.groupBy("mths_since_last_major_derog").count())

# COMMAND ----------

from pyspark.sql.functions import array, col, explode, lit, struct

display(
  raw
    .select(explode(array(*[struct(lit(c).alias("k"), col(c).alias("v")) for c in int_cols + double_cols])).alias("col"))
    .where(~col("col.v").rlike("^[0-9\. ]+$"))
    .groupBy("col.k", "col.v")
    .count()
    .orderBy(col("count").desc()))

# COMMAND ----------

int_exprs = [col(c).cast("int") if c in int_cols else col(c) for c in raw.columns]
double_exprs = [col(c).cast("double") if c in double_cols else col(c) for c in raw.columns]

typed = raw.select(double_exprs).select(int_exprs).drop(*drop_cols)

typed.printSchema()

# COMMAND ----------

# MAGIC %md Fill numerical columns with zeros

# COMMAND ----------

typed_filled = typed.na.fill({c: 0 for c in int_cols + double_cols})

# COMMAND ----------

# MAGIC %md Remove columns which have high number of missing values

# COMMAND ----------

from pyspark.sql.functions import mean
import pandas as pd

def drop_low_quality_columns(df, threshold = 0.5, subset = None):
  """Drop columns which have fraction of  NULLs higher than threshold"""
  cols = df.columns if subset is None else subset
  missing_fractions = (df
                       .select([mean(col(c).isNull().cast("integer")).alias(c) for c in cols])
                       .first().asDict())
  to_drop = [c for c, fract in missing_fractions.items() if fract >= threshold]
  return df.drop(*to_drop), spark.createDataFrame(pd.DataFrame(missing_fractions.items()), ("column", "fraction"))


# COMMAND ----------

good, missing_fractions = drop_low_quality_columns(typed_filled )

good.printSchema()

# COMMAND ----------

display(missing_fractions)

# COMMAND ----------

display(raw.select("annual_inc_joint").distinct())



# COMMAND ----------

# MAGIC %md Encode label column

# COMMAND ----------

# Encode label
from pyspark.sql.functions import when, upper, trim

def encode_loan_status(c):
  not_delinquent = {'CURRENT', 'IN GRACE PERIOD', 'LATE (16-30) DAYS', 'LATE (31-120 DAYS)', 'ISSUED'}
  delinquent = {'DEFAULT', 'CHARGED OFF', 'DOES NOT MEET THE CREDIT POLICY. STATUS:CHARGED OFF'}
  paid = {'FULLY PAID', 'DOES NOT MEET THE CREDIT POLICY. STATUS:FULLY PAID'}
  c = upper(trim(col(c)))
  return when(c.isin(delinquent), "DELINQUENT").when(c.isin(not_delinquent), "NOT DELINQUENT").otherwise("PAID")

good_with_label = good.withColumn("loan_status", encode_loan_status("loan_status"))

# COMMAND ----------

# MAGIC %md Check number of unique value in the remaining categorical columns

# COMMAND ----------

from pyspark.sql.functions import approx_count_distinct

selected_string_cols = [c for c in string_cols if c in good_with_label.columns if c != "loan_status"]

display(good_with_label.select([approx_count_distinct(c).alias(c) for c in selected_string_cols]))  # Note: Features with only one level should be skipped

# COMMAND ----------

# MAGIC %md  Number of distinct applications types looks surprisingly high, so let's check it.

# COMMAND ----------

display(good_with_label.groupBy("application_type").count().orderBy(col("count").desc()))

# COMMAND ----------

# MAGIC %md Let's remove unexpected values

# COMMAND ----------

good_with_label_and_application_type = good_with_label.withColumn("application_type", when(col("application_type").isin({'INDIVIDUAL', 'JOINT'}), col("application_type")))

# COMMAND ----------

# MAGIC %md Fraction of missing values is relatively low so we could drop NULL without significant loss. 

# COMMAND ----------

good_with_label_and_application_type.count(), good_with_label_and_application_type.na.drop().count(), good_with_label_and_application_type.na.drop(subset = selected_string_cols).count()

# COMMAND ----------

# MAGIC %md In Spark 2.2 we could use `pypark.ml.feature.Imputer` on `double_cols`

# COMMAND ----------

if spark.version > "2.2.0":

  from pyspark.ml.feature import Imputer

  selected_double_cols = [c for c in double_cols if c in good_with_label_and_application_type.columns]
  selected_double_cols_imp = ["{}_imp".format(c) for c in selected_double_cols]

  imputer = Imputer(inputCols = selected_double_cols, outputCols = selected_double_cols_imp)

  imputer_model = imputer.fit(good_with_label_and_application_type)
  imputed = imputer_model.transform(good_with_label_and_application_type)

# COMMAND ----------

# MAGIC %md but it won't give us much here.

# COMMAND ----------

if spark.version > "2.2.0":

  print(imputed.na.drop().count())

# COMMAND ----------

good_without_na = good_with_label_and_application_type.na.drop()
display(good_without_na)

# COMMAND ----------

# MAGIC %md Extract year

# COMMAND ----------

from pyspark.sql.functions import regexp_extract

good_with_year = good_without_na.withColumn("issue_d", regexp_extract("issue_d", ".*-(20[0-9]{2})$", 1))

# COMMAND ----------

# MAGIC %md Finally let's save the result as a persisent table

# COMMAND ----------

good_with_year.write.format("parquet").mode("overwrite").saveAsTable("loans_clean")
